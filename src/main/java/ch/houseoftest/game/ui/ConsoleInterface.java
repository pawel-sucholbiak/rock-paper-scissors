package ch.houseoftest.game.ui;

import ch.houseoftest.game.GameType;
import ch.houseoftest.game.Handsign;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class ConsoleInterface implements UserInterfaceIface {
    private static final String OPTION_FORMAT = "[%d] %s\n";
    private static final String WRONG_NUMBER_MESSAGE = "Sorry, I can't understand that. " +
            "Next time, please choose the correct number from the list.\n";
    private static final String INTRO_MESSAGE = "Hello Player.\nLet's play Rock-Paper-Scissors game.\n";
    private static final String GAME_TYPE_QUESTION = "What game type would you like to play?\n";
    private static final String HANDSIGN_QUESTION = "Which handsign do you pick?\n";
    private static final String GOODBYE_MESSAGE = "Thanks for playing. Bye.";
    private static final String DRAW_RESULT_MESSAGE_FORMAT = "It's a DRAW.\nBoth sides have chosen %s\n";
    private static final String COMPUTER_WIN_RESULT_MESSAGE_FORMAT = "%s WON.\n";
    private static final String PLAYER_WIN_RESULT_MESSAGE = "You WON! Congratulations.\n";
    private static final String PLAYER_LOSE_RESULT_MESSAGE = "Sorry, you've lost.\n";
    private static final String WHAT_BEATS_WHAT_MESSAGE_FORMAT = "%s beats %s\n";
    private static final String MISSING_GAME_TYPE_IMPL_MESSAGE = "Sorry, we're still working on that Game Type." +
            " Please try to pick another one.\n";
    private static final String SPACER = "------------------------------\n\n";

    private final OutputStream outputStream;
    private final InputStream inputStream;

    public ConsoleInterface(InputStream inputStream, OutputStream outputStream) {
        this.inputStream = inputStream;
        this.outputStream = outputStream;
    }

    public void displayIntro() {
        print(INTRO_MESSAGE);
    }

    public GameType askAndGetGameTypeFromUser() {
        int gameId = getIntegerFromUserInput(formulateGameTypeQuestion());
        try {
            return GameType.getById(gameId);
        } catch (IllegalArgumentException ex) {
            // log ex
            print(WRONG_NUMBER_MESSAGE);
            throw new UserInputException("Unknown game type");
        }
    }

    public Handsign askAndGetHandsignFromUser() {
        int handsignId = getIntegerFromUserInput(formulateHandsignToChooseQuestion());
        try {
            return Handsign.getById(handsignId);
        } catch (IllegalArgumentException ex) {
            // log ex
            print(WRONG_NUMBER_MESSAGE);
            throw new UserInputException("Unknown handsign");
        }
    }

    public void informAboutUnknownGameType() {
        print(MISSING_GAME_TYPE_IMPL_MESSAGE);
    }

    String formulateGameTypeQuestion() {
        StringBuilder sb = new StringBuilder();
        sb.append(GAME_TYPE_QUESTION);
        for (GameType gameType : GameType.values()) {
            sb.append(String.format(OPTION_FORMAT, gameType.getId(), gameType.getLabel()));
        }
        return sb.toString();
    }

    String formulateHandsignToChooseQuestion() {
        StringBuilder sb = new StringBuilder();
        sb.append(HANDSIGN_QUESTION);
        for (Handsign handsign : Handsign.values()) {
            sb.append(String.format(OPTION_FORMAT, handsign.getId(), handsign.name()));
        }
        return sb.toString();
    }

    private void print(String message) {
        try {
            outputStream.write(message.getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            //log issue with outputStream
        }
    }

    private int getIntegerFromUserInput(String question) {
        Scanner scanner = new Scanner(inputStream);
        print(question);
        try {
            return scanner.nextInt();
        } catch (NoSuchElementException | IllegalStateException exception) {
            print(WRONG_NUMBER_MESSAGE);
            throw new UserInputException("User provided unexpected value");
        }
    }

    public void sayGoodbye() {
        print(GOODBYE_MESSAGE);
    }

    public void communicateDraw(Handsign handsign) {
        print(String.format(DRAW_RESULT_MESSAGE_FORMAT, handsign));
        print(SPACER);
    }

    public void communicateComputersWin(String name, Handsign winningHandsign, Handsign loosingHandsign) {
        print(String.format(COMPUTER_WIN_RESULT_MESSAGE_FORMAT, name));
        sayWhatBeatsWhat(winningHandsign, loosingHandsign);
        print(SPACER);
    }

    public void communicatePlayerWin(Handsign winningHandsign, Handsign loosingHandsign) {
        print(PLAYER_WIN_RESULT_MESSAGE);
        sayWhatBeatsWhat(winningHandsign, loosingHandsign);
        print(SPACER);
    }

    public void communicatePlayerLose(Handsign winningHandsign, Handsign loosingHandsign) {
        print(PLAYER_LOSE_RESULT_MESSAGE);
        sayWhatBeatsWhat(winningHandsign, loosingHandsign);
        print(SPACER);
    }

    private void sayWhatBeatsWhat(Handsign winningHandsign, Handsign loosingHandsign) {
        print(String.format(WHAT_BEATS_WHAT_MESSAGE_FORMAT, winningHandsign, loosingHandsign));
    }
}
