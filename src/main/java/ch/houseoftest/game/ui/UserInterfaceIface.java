package ch.houseoftest.game.ui;

import ch.houseoftest.game.GameType;
import ch.houseoftest.game.Handsign;

public interface UserInterfaceIface {
    void displayIntro();

    GameType askAndGetGameTypeFromUser();

    void sayGoodbye();

    void communicateDraw(Handsign handsign);

    void communicateComputersWin(String name, Handsign winningHandsign, Handsign loosingHandsign);

    void communicatePlayerWin(Handsign winningHandsign, Handsign loosingHandsign);

    void communicatePlayerLose(Handsign winningHandsign, Handsign loosingHandsign);

    Handsign askAndGetHandsignFromUser();

    void informAboutUnknownGameType();
}
